const Sequelize = require('sequelize');
const dbConfig = require("../config/database");
const Answer = require('../models/Answer');

const Companny = require("../models/Companny");
const Form = require('../models/Form');
const Question = require('../models/Question');

const User = require("../models/User")

require('dotenv/config');



const connection = new Sequelize(process.env.DATABASE_URL , dbConfig)

Companny.init(connection)
User.init(connection)
Form.init(connection)
Question.init(connection)
Answer.init(connection)



User.associate(connection.models)
Companny.associate(connection.models)
Form.associate(connection.models)
Question.associate(connection.models)
Answer.associate(connection.models)



module.exports = connection;