'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('answers', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
        user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: "users", key: "id" },
        onUpdate:"CASCADE",
        onDelete:"CASCADE"
      },
      answered_by:{
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: "users", key: "id" },
        onUpdate:"CASCADE",
        onDelete:"CASCADE"
      },
      form_id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: "forms", key: "id" },
        onUpdate:"CASCADE",
        onDelete:"CASCADE"
      },
      companny_id:{
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: "compannies", key: "id" },
        onUpdate:"CASCADE",
        onDelete:"CASCADE"
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
     });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('answers');

  }
};
