'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('questions', 'answer_id', {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: { model: "answers", key: "id" },
      onUpdate:"CASCADE",
      onDelete:"CASCADE"
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('questions', 'answer_id');

  }
};
