'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('compannies', 'form_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: { model: "forms", key: "id" },
      onUpdate:"CASCADE",
      onDelete:"CASCADE"
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('compannies', 'form_id');
  }
};
