const { Model, DataTypes } = require("sequelize");

class User extends Model {
    static init(sequelize){
        super.init({
            name:DataTypes.STRING,
            cpf:DataTypes.STRING,
            numero:DataTypes.STRING,
            office:DataTypes.STRING
        }, {
            sequelize
        })
    }
    static associate(models){
        this.belongsTo(models.Companny, {
            foreignKey:'companny_id',
            as: "companny"
        })
        this.hasMany(models.Answer, {
            foreignKey:'user_id',
            as: "aswersAboutMe"
        })
        this.hasMany(models.Answer, {
            foreignKey:'answered_by',
            as: "aswers"
        })
    }
    
}

module.exports = User;