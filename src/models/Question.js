const { Model, DataTypes } = require("sequelize");

class Question extends Model {
    static init(sequelize){
        super.init({
            question:DataTypes.STRING,
            answer:DataTypes.INTEGER,
            category:DataTypes.STRING
        }, {
            sequelize
        })
    }
    static associate(models){
        this.belongsTo(models.Answer, {
            foreignKey:'answer_id',
            as: "registered_answer"
        })
    }
    
}

module.exports = Question;