const { Model, DataTypes } = require("sequelize");

class Companny extends Model {
    static init(sequelize){
        super.init({
            cnpj: DataTypes.STRING, 
            corporate_name: DataTypes.STRING, 
            fantasy_name: DataTypes.STRING,
            zip_code: DataTypes.STRING, 
            number: DataTypes.STRING, 
            street: DataTypes.STRING, 
            district: DataTypes.STRING
        }, {
            sequelize
        })
    }

    static associate(models){
        this.hasMany(models.User, {
            foreignKey:'companny_id',
            as: "users"
        })
        this.belongsTo(models.Form, {
            foreignKey:'form_id',
            as: "form"
        })
        this.hasMany(models.Answer, {
            foreignKey:'companny_id',
            as: "answers"
        })
    }
}

module.exports = Companny;