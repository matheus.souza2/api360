const { Model, DataTypes } = require("sequelize");

class Answer extends Model {
    static init(sequelize){
        super.init({

        }, {
            sequelize
        })
    }
    static associate(models){
        this.belongsTo(models.Form, {
            foreignKey:'form_id',
            as: "form"
        })
        this.belongsTo(models.Companny, {
            foreignKey:'companny_id',
            as: "companny"
        })
        this.belongsTo(models.User, {
            foreignKey:'user_id',
            as: "user"
        })
        this.belongsTo(models.User, {
            foreignKey:'answered_by',
            as: "answeredBy"
        })
        this.hasMany(models.Question, {
            foreignKey:'answer_id',
            as: "questions"
        })
    }
    
}

module.exports = Answer;