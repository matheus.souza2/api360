const { Model, DataTypes } = require("sequelize");

class Form extends Model {
    static init(sequelize){
        super.init({
              name:DataTypes.STRING,
              questions:DataTypes.JSON
        }, {
            sequelize
        })
    }
    static associate(models){
        this.hasMany(models.Companny, {
            foreignKey:'form_id',
            as: "companny"
        })
        this.hasMany(models.Answer, {
            foreignKey:'form_id',
            as: "answers"
        })
    }
}

module.exports = Form;