const Companny = require("../models/Companny");
const User = require("../models/User");

const Form = require("../models/Form");
const Question = require("../models/Question");
const Answer = require("../models/Answer");
module.exports = {
  async indexCompanny(req, res) {
    let { companny, form } = req.params;

    const answers = await Answer.findAll({
      where: {
        companny_id: Number(companny),
        form_id: Number(form),
      },
      include: { association: "questions" },
    });

    if (!answers) {
      return res.status(400).json({ erro: "answers not found" });
    }

    return res.json(answers);
  },

  async indexUser(req, res) {
    let { companny, form, user } = req.params;

    const answers = await Answer.findAll({
      where: {
        companny_id: Number(companny),
        user_id: Number(user),
        form_id: Number(form),
      },
      include: [{ association: "questions" }, { association: "user" }],
    });

    if (!answers) {
      return res.status(400).json({ erro: "answers not found" });
    }

    return res.json(answers);
  },

  async getUsers(req, res) {
    let { companny, form, user } = req.params;
    const users = await User.findAll({
      where: {
        companny_id: Number(companny),
      },
    });
    const answers = await Answer.findAll({
      where: {
        companny_id: Number(companny),
        form_id: Number(form),
        answered_by: Number(user),
      },
      include: [{ association: "user" }],
    });
    if (!answers) {
      return res.status(400).json({ erro: "answers not found" });
    }

    return res.json(answers);
  },

  async store(req, res) {
    const { questions, user_id, answered_by, form_id, companny_id } = req.body;

    const answers = await Answer.findOne({
      where: { user_id, answered_by, form_id, companny_id },
    });

    if (answers) {
      return res.status(400).json({ msg: "answer already exists" });
    }

    const answer = await Answer.create({
      user_id,
      answered_by,
      form_id,
      companny_id,
    });
    for (let q in questions) {
      questions[q].answer_id = answer.dataValues.id;
    }

    await Question.bulkCreate(questions)
      .then(() => {
        // Notice: There are no arguments here, as of right now you'll have to...
        return Question.findAll();
      })
      .then((questions) => {
        return res.json({ msg: "criado com sucess" }); // ... in order to get the array of user objects
      });
  },
};
