const sequelize = require("sequelize");
const Answer = require("../models/Answer");
const Companny = require("../models/Companny");
const Question = require("../models/Question");
const User = require("../models/User");
const { Op } = require("sequelize");

module.exports = {
  async index(req, res) {
    const { cpf } = req.body;

    const user = await User.findAll({
      where: { cpf },
      include: { association: "companny" },
    });
    console.log(user);
    if (!user) {
      return res.status(404).json({ erro: "Cpf não cadastrado!" });
    }

    return res.json(user);
  },
  async indexAll(req, res) {

    const user = await User.findAll({
      include: { association: "companny" },
    });


    return res.json(user);
  },

  async indexWithId(req, res) {
    const { user_id } = req.params;
    const user = await User.findByPk(user_id);

    // const questions = await Question.findAll({
    //   where: { answered_by: user_id },
    // });

    if (!user) {
      return res.status(400).json({ erro: "user not found" });
    }
    // var remainsToAnswer = [];

    // for (let q of questions) {
    //   if (remainsToAnswer.indexOf(q.user_id) === -1) {
    //     remainsToAnswer.push(q.user_id);
    //   }
    // }

    return res.json(user );
  },

  async indexWithCompannyId(req, res) {
    const { companny_id } = req.params;
    const user = await User.findAll({where:{companny_id}});

    // const questions = await Question.findAll({
    //   where: { answered_by: user_id },
    // });

    if (!user) {
      return res.status(400).json({ erro: "user not found" });
    }
    // var remainsToAnswer = [];

    // for (let q of questions) {
    //   if (remainsToAnswer.indexOf(q.user_id) === -1) {
    //     remainsToAnswer.push(q.user_id);
    //   }
    // }

    return res.json(user );
  },
  async listUsersToAnswer(req, res) {
    const { user_id, companny_id, form_id } = req.params;
    const remainsToAnswer = [];

    const answers = await Answer.findAll({
      where: { answered_by: user_id, form_id, companny_id },
    });
    let a = []

    for await( let i of answers){
      a.push(i.user_id)
    }

    const user = await User.findAll({where:{id:{[Op.notIn]: a,[Op.ne]: user_id}, companny_id}});


    return res.json(user);
  },

  async store(req, res) {
    const { name, cpf, office, numero, companny_id } = req.body;

    const companny = await Companny.findByPk(companny_id);

    const compareteUser = await User.findAll({ where: { cpf } });

    if (compareteUser.length > 0) {
      for (let u of compareteUser) {
        if (u.companny_id == companny_id) {
          return res
            .status(400)
            .json({ erro: "user already exists in this companny" });
        }
      }
    }

    if (!companny) {
      return res.status(400).json({ erro: "companny not found" });
    }

    const user = await User.create({
      name,
      cpf,
      office,
      numero,
      companny_id,
    });

    return res.json(user);
  },
  async edit(req, res){
    const { name, cpf, office, numero } = req.body;
    const {id} = req.params
  
    user = await User.update( { name, cpf, office, numero}, {where:{id}})

    return res.json(user)
  },
  async delete(req, res){

    const {id} = req.params
    const user = await User.destroy({where:{id}})

    return res.json(user)
},
};
