const Companny = require("../models/Companny")
module.exports = {
    async store(req, res){
        const {cnpj, corporate_name, fantasy_name,zip_code, number, street, district, form_id } = req.body;
        // console.log({cnpj, corporate_name, fantasy_name,zip_code, number, street, distict })

        if(await Companny.findOne({where:{cnpj}})){
            return res.status(400).json({message:"cnpj já cadastrado!"})
        }
        
        const companny = await Companny.create({cnpj, corporate_name, fantasy_name,zip_code, number, street, district, form_id })

        return res.json(companny)
    },
    async update(req, res){
        const { cnpj, corporate_name, fantasy_name,zip_code, number, street, district, form_id } = req.body;
        const {id} = req.params
        // console.log({cnpj, corporate_name, fantasy_name,zip_code, number, street, distict })
        const existCompanny = await Companny.findAll({where:{cnpj}})

        if(existCompanny[0] && existCompanny[0].id != id){
            return res.status(400).json({message:"cnpj já cadastrado!", existCompanny})
        }
        
        const companny = await Companny.update({cnpj, corporate_name, fantasy_name,zip_code, number, street, district, form_id }, {where:{id}})

        return res.json(companny)
    },
    async delete(req, res){

        const {id} = req.params
        const companny = await Companny.destroy({where:{id}})

        return res.json(companny)
    },

    async index(req, res){
        const {companny_id} = req.params;

        const companny = await Companny.findByPk(companny_id)

        return res.json(companny)
    },

    async indexAll(req, res){
        // const {companny_id} = req.params;

        const compannies = await Companny.findAll()

        return res.json(compannies)
    }
}