const Companny = require("../models/Companny");
const User = require("../models/User");

const Form = require("../models/Form");
module.exports = {
  async index(req, res) {
    const { cpf } = req.body;

    const user = await User.findAll({where: {cpf}});
    if (!user) {
        return res.status(400).json({ erro: "user not found" });
    }
    
    return res.json(user);

  },
  async indexId(req, res) {
    const {id} =req.params

    const forms = await Form.findByPk(id);

    
    return res.json(forms);

  },
  async indexAll(req, res) {

    const forms = await Form.findAll();

    
    return res.json(forms);

  },

  async store(req, res) {
    const {
        name, questions
    } = req.body;

    const form = await Form.create({
        name, questions
    });

    return res.json(form);
  },
  async update(req, res) {
    const {id} =req.params

    const {
        name, questions
    } = req.body;

    const form = await Form.update({
        name, questions
    }, {where:{id}});

    return res.json(form);
  },
  async delete(req, res){
    const {formId} = req.params

    const compannies = await Companny.findAll({where:{form_id:Number(formId)}})
    for(let i of compannies){
      await Companny.update({form_id:null}, {where:{id:i.id}})
    }

    await Form.destroy({where:{id:formId}})

    return res.json(compannies)
  }
  
};
