const Companny = require("../models/Companny");
const User = require("../models/User");

const Form = require("../models/Form");
const Question = require("../models/Question");
const Answer = require("../models/Answer");
module.exports = {
  async index(req, res) {

    const user = await Question.findAll({
      include: { association: "registered_answer" },
    });

    if (!user) {
      return res.status(400).json({ erro: "user not found" });
    }

    return res.json(user);
  },

  async result(req, res) {
    const { userId } = req.params;
    
  },

  async store(req, res) {
    const { questions, user_id, answered_by, form_id, companny_id } = req.body;

    const answers = await Answer.findOne({
      where: { user_id, answered_by, form_id, companny_id },
    });

    if (answers) {
      return res.status(400).json({ msg: "answer already exists" });
    }

    const answer = await Answer.create({
      user_id,
      answered_by,
      form_id,
      companny_id,
    });
    for (let q in questions) {
      questions[q].answer_id = answer.dataValues.id;
    }

    await Question.bulkCreate(questions)
      .then(() => {
        // Notice: There are no arguments here, as of right now you'll have to...
        return Question.findAll();
      })
      .then((questions) => {
        return res.json({ msg: "criado com sucess" }); // ... in order to get the array of user objects
      });
  },
};
