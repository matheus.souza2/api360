require('dotenv/config');
module.exports = {
    dialect:process.env.DIALECT,
    host:process.env.DATABASE_URL.split("@")[1].split(":")[0],
    username:process.env.DATABASE_URL.split("//")[1].split(":")[0],
    database:process.env.DATABASE_URL.split("/")[3],

    define:{
        timestamps:true,
        underscored:true,
    }
}