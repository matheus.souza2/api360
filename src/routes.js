const { Router } = require("express");
const express = require("express");
const AnswerController = require("./controllers/AnswerController");

const routes = express.Router();

const CompannyController = require("./controllers/CompannyController");
const FormController = require("./controllers/FormController");
const QuestionController = require("./controllers/QuestionController");
const UserController = require("./controllers/UserController")


routes.get("/", (req, res)=>{
    return res.json({hello:"helloworld"})
})

routes.post("/create/companny", CompannyController.store)
routes.get("/companny/:companny_id/", CompannyController.index)
routes.get("/compannies/", CompannyController.indexAll)
routes.put("/update/companny/:id", CompannyController.update)
routes.delete("/delete/companny/:id", CompannyController.delete)


routes.post("/create/users", UserController.store)
routes.post("/user/", UserController.index)
routes.get("/user/list/:user_id/", UserController.indexWithId)
routes.get("/user/list/companny/:companny_id/", UserController.indexWithCompannyId)
routes.get("/user/list/", UserController.indexAll)
routes.get("/user/listuserstoanswer/:user_id/:companny_id/:form_id", UserController.listUsersToAnswer)
routes.put("/user/edit/:id", UserController.edit)
routes.delete("/user/delete/:id", UserController.delete)


routes.post("/form/create/", FormController.store)
routes.put("/form/update/:id", FormController.update)
routes.get("/form/list/", FormController.indexAll)
routes.get("/form/list/:id", FormController.indexId)
routes.delete("/form/delete/:formId", FormController.delete)

routes.post("/questions/create/", QuestionController.store)
routes.get("/questions/list/", QuestionController.index)


routes.get("/answers/list/companny/:companny/:form", AnswerController.indexCompanny)
routes.get("/users/list/answers/:companny/:form/:user", AnswerController.getUsers)
routes.get("/answers/list/user/:companny/:form/:user", AnswerController.indexUser)





module.exports = routes;