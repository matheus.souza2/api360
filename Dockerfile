FROM alpine:latest

EXPOSE 3333

WORKDIR /srv

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]

CMD ["/usr/bin/node", "src/server.js"]

RUN apk add dumb-init yarn && \
    adduser -D -h /srv srv && \
    chown -R srv:srv /srv

USER srv

COPY --chown=srv:srv ./package.json yarn.lock /srv/

RUN yarn install --production

COPY --chown=srv:srv . /srv
